/* PMJ BS Bingo
 * Copyright (C) 2021 PMJ Rocks (https://pmj.rocks)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const words = `


bessere Handhabe für Behörden
Islam
Jihad
die Gewaltenteilung ist gewährleistet
Sicherheit UND Freiheit stärken
gesetzliche Lücke schliessen
Furcht und Schrecken
Schutz von unschuldigen Bürger*innen
man kann sich dagegen wehren
beeinflussbare Jugendliche
die Schweiz ist ein Rechtsstaat
eine Reihe von Massnahmen
Morges
Lugano
es gibt keine Datenbank
Terrorismusdefinition aus NDG
das Gesetz ist klar
die Terrorismusdefinition ist überall die selbe
wenige Einzelfälle
Linksextremismus
Augenmass
Ich verspreche ihnen dass...
Verhältnismässigkeit
das Gesetz wurde vorher geprüft


`;
