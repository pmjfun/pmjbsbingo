# PMJ BS Bingo

The PMJ BS Bingo is a customizable bullshit bingo game with [Bootstrap 4.6](https://getbootstrap.com/docs/4.6/getting-started/download/).  
It is a simple Webapplication written in PHP and Javascript.

It is a full rewrite of [domahidizoltan](https://github.com/domahidizoltan)'s [Bullshit Bingo](https://github.com/domahidizoltan/bullshit-bingo)!

It was created for the [Swiss Pirate Party](https://www.piratenpartei.ch/) and its outlet Cyberstammtisch to add some fun while watching a weekly politcal entertainment show on Swiss national television called Arena.  
That's the reason why it is PPS branded.

---

## Installation

Just clone the repository in your webroot and edit the config variables in index.php to your liking.

Then you can edit js/words.js to fill the fields.

You can also use your own compiled Bootstrap CSS.

## How it works

Well, if you don't know how a Bullshit Bingo works, then this script probably isn't for you :P  
Really, you just click on a field as soon as the phrase is said and if you have all fields in a row or in a column marked, the row or column turns green and you have a BINGO!