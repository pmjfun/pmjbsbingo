## Changelog

### 1.0

#### 1.0.1 [2021-05-21]

* Update changelog to include last commit
* Update index.php for shareness

#### 1.0.0 [2021-05-20]

* Inital working version